# Test-shared-library

Libraire partagée afin d'automatiser les tâches générées avec des pipelines dans Jenkins.


## Requirements
#### Plugins
	Pipeline (and dep)
	Pipeline Maven Integration Plugin
	Slack Notification Plugin
	Pipeline Utility Steps

## Jenkinsfile
Le Jenkinsfile peut être défini sous cette forme :

    mainPipeline {
        repository = "<repository git url>"
        branch = "<repository branch>"
        env = "<dev | preprod | prod>"
        domain = "<application domain>" (optional)
        maven = true (optional)
        dirMaven = "<Maven's project root folder>" (optional)
        mavenArgs = "<default : clean install -DskipTests>" (optional)
        dirDocker = "<Docker's project root folder>" (optional)
        node = true (optional)
        slackChannel = "<default : #integration_continue>" (optional)
        dependencies = < n number of dependencies > (optional)
        dep1 = <repository git url> <repository branch> <dev | preprod | prod> <folder name> (optional)
        ...
        depn = <repository git url> <repository branch> <dev | preprod | prod> <folder name> (optional)
    }

Ceci va permettre de lancer une image personnalisée dans un nœud esclave afin d’effectuer le déploiement du projet.

 - **repository**  Lien du dépôt du projet principal qui va être déployé
 - **branch** La branche du dépôt qui devra être utilisé
 - **env** L'environnement de production
 - **domain** Domaine d'application du projet
 - **maven** Active la compilation maven  
 - **dirMaven** Chemin vers la racine du projet maven si différent de la racine du dépôt *( chemin vers le pom.xml )*
 - **mavenArgs** Arguments supplémentaires pour la compilation avec maven

 - **dirDocker** Chemin vers la racine du projet docker si différent de la racine du dépôt *( chemin vers le Dockerfile )*
 - **node** Active la compilation d'un projet node *( utilisation d'une image plus légère compatible avec un projet node )*
 - **slackChannel** Chaîne du Slack pour les messages concernant le déploiement
 - **dependencies** Nombre de dépendances n devant être compilées pour le projet principal
 - **dep1** Projet devant être compilé
	 - repository git url :  Lien du dépôt
	 - repository branch : La branche du dépôt
	 - dev | preprod | prod : L'environnement de production
	 - folder name : Chemin vers la racine du projet maven *( un espace si la racine se trouve à la racine du dépôt )*


## Exemples

    mainPipeline {
                repository = "https://example@bitbucket.org/example/project.git"
                branch = "master"
                env = "dev"
    }

Le comportement par défaut de la librairie considère que le projet devra être compilé avec Docker avec un Dockerfile se trouvant à la racine du dépôt.

    mainPipeline {
        repository = "https://example@bitbucket.org/example/project.git"
        branch = "master"
        env = "dev"
        maven = true
        dirMaven = "/home/project/maven"
        slackChannel = "#projects"
        dependencies = 3
        dep1 ="https://example@bitbucket.org/example/depend1.git master dev depend1"
        dep2 ="https://example@bitbucket.org/example/depend2.git master dev  "
        dep3 ="https://example@bitbucket.org/example/depend2.git master dev depend2"
    }

La librairie va d'abord compiler les dépendances puis va effectuer le déploiement du projet principal avec maven pour ce cas-ci.

## Déroulement d'un déploiement

![Alt text](https://image.noelshack.com/fichiers/2018/25/3/1529505985-mermaid.png)